//
// Created by glagos on 14-07-21.
//

#ifndef OMUAMUA_WINDOW_H
#define OMUAMUA_WINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "speed.h"


struct WindowData {
    int width;
    int height;
};

static struct WindowData InstanceWindowData;

struct WindowData* getWindowData();
void initializeWindow(GLFWmonitor* monitor);
void adjustWindow(GLFWwindow* window);
void handleAdjustWindow(GLFWwindow* window, int width, int height);


#endif //OMUAMUA_WINDOW_H

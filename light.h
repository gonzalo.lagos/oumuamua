//
// Created by glagos on 20-07-21.
//

#ifndef OMUAMUA_LIGHT_H
#define OMUAMUA_LIGHT_H

typedef struct LightDataStruct {
    int pointLightingLocationUniform;
    int pointLightingColorUniform;

    int isLightEmmiterUniform;
    int isLightReceiverUniform;
} LightData;

static LightData InstanceLightData;

LightData* getLight();
void setLightUniforms(unsigned int program);


#endif //OMUAMUA_LIGHT_H

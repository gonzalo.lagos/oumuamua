//
// Created by glagos on 14-07-21.
//

#include "object.h"
#include "util.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string.h>

#include <jansson.h>
#include "shader.h"
#include "light.h"
#include "matrix.h"
#include "buffer.h"
#include "speed.h"
#include "util.h"


struct _OumuamuaObject {
    GObject parent_instance;

    gchar*   name;
    gchar*   type;

    float   radius;
    float   compression;
    float   inclination;
    float   tilt;
    float   revolution;
    float   rotation;
    float   distance;
    bool    center;
    bool    radiate;
    bool    cosmic;

    GList  *objects;   // OumuamuaObject

};

G_DEFINE_TYPE (OumuamuaObject, oumuamua_object, G_TYPE_OBJECT)


static void
oumuamua_object_class_init (OumuamuaObjectClass *klass)
{

}

static void
oumuamua_object_init (OumuamuaObject *self)
{
  self->objects = NULL;
}

OumuamuaObject *
oumuamua_object_new (void)
{
  OumuamuaObject *object;

  object = g_object_new (OUMUAMUA_TYPE_OBJECT, NULL);
  return object;
}



float normalizeObjectSize(float size) {
    return (float)(sqrtf(size) * ConfigObjectFactorSize);
}

unsigned int getObjectDrawMode(Object* object) {
    if (strcmp(object->type, "circle") == 0) {
        return GL_LINES;
    }
    return GL_TRIANGLES;
}

Object *loadObjects(char *mapName) {

    if (strcmp(mapName, "stellar") == 0)
        return loadStellar();
    else if (strcmp(mapName, "voidbox") == 0)
        return loadVoidbox();
    else
        return NULL;
}

void renderObjects(Object *objects, unsigned int program) {
    // Acquire shader
    ShaderData*         shader          = getShader();
    LightData*          light           = getLight();
    MatrixUniforms*     matrixUniforms  = getMatrixUniforms();

    // Iterate on current-level objects
    // Object* o = objects;
    int i = 0;
    while((objects + i) != NULL) {
        Object* o = objects + i;
        Buffers* buffers = getBuffersFromMap(o->name);

        glBindTexture(GL_TEXTURE_2D, buffers->texture.ref);
        // Toggle to child context
        pushMatrix();

        // Update angles for object
        addToBufferAngleRotation(buffers, rotationAngleSinceLast(o));
        addToBufferAngleRevolution(buffers, revolutionAngleSinceLast(o));
        // Apply model transforms
        mat4* currentMatrixShared = getMatrix();

        if (o->tilt != 0) {
            vec3 axis = {1, 0, 0};
            mat4* homogRotated3D;
            HomogRotate3D(buffers->angleTilt, &axis, homogRotated3D);
            glm_mat4_mul(*currentMatrixShared, *homogRotated3D, *currentMatrixShared);
        }

        if (o->revolution != 0) {
            vec3 axis = {0, 1, 0};
            mat4* homogRotated3D;
            HomogRotate3D(buffers->angleRevolution, &axis, homogRotated3D);
            glm_mat4_mul(*currentMatrixShared, *homogRotated3D, *currentMatrixShared);
        }

        if (o->distance > 0 && o->center != true)
        {
            mat4* translated;
            glm_mat4_zero(*translated);
            float normObjSize = normalizeObjectSize(o->distance);
            Translate3D(normObjSize, 0.0f, 0.0f, *translated);
            glm_mul(*currentMatrixShared, *translated, *currentMatrixShared);
        }

        setMatrix(currentMatrixShared);

        // Toggle to unary context
        pushMatrix();

        // Apply object angles
        mat4* currentMatrixSelf = getMatrix();

        if (o->inclination > 0) {
            vec3 axis = {0, 0, 1};
            mat4* homogRotated3D;
            HomogRotate3D(o->inclination / 90.0f, &axis, homogRotated3D);
            glm_mat4_mul(*currentMatrixShared, *homogRotated3D, *currentMatrixShared);
        }

        if (o->rotation != 0) {
            vec3 axis = {0, 1, 0};
            mat4* homogRotated3D;
            HomogRotate3D(buffers->angleRotation, &axis, homogRotated3D);
            glm_mat4_mul(*currentMatrixShared, *homogRotated3D, *currentMatrixShared);
        }

        // Process normal to model matrix
        // normalMatrix := mgl32.Mat4Normal(*currentMatrixSelf);
        mat3* normalMatrix;
        glm_mat3_zero(*normalMatrix);
        Mat4Normal(*currentMatrixSelf, *normalMatrix);

        // Apply model + normal
        glUniformMatrix4fv(matrixUniforms->model, 1, false, *currentMatrixSelf[0]);
        glUniformMatrix3fv(matrixUniforms->normal, 1, false, *normalMatrix[0]);

        // Render vertices
        glBindBuffer(GL_ARRAY_BUFFER, buffers->VBOElementVertices);
        glVertexAttribPointer(shader->vertexAttributes, 3, GL_FLOAT, false, 0, 0);
        // gl.VertexAttribPointer(shader.VertexAttributes, 3, gl.FLOAT, false, 0, gl.PtrOffset(0))

        // Render textures
        glBindBuffer(GL_ARRAY_BUFFER, buffers->VBOElementTexture);
        glVertexAttribPointer(shader->vertexTextureCoords, 2, GL_FLOAT, false, 0, 0);

        // Render vertice lightings
        glBindBuffer(GL_ARRAY_BUFFER, buffers->VBOElementVerticesNormals);
        glVertexAttribPointer(shader->normalAttributes, 3, GL_FLOAT, false, 0, 0);

        // Light emitter? (eg: Sun)
        if (o->radiate == true) {
            glUniform1i(light->isLightEmmiterUniform, 1);
            glUniform3f(light->pointLightingLocationUniform, 0, 0, 0);
            glUniform3f(light->pointLightingColorUniform, 1, 1, 1);
        }

        // Render indices
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers->VBOElementIndices);
        // gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.VBOElementIndices)

        // Draw elements
        glDrawElements(getObjectDrawMode(o), lenIntArray(buffers->element.indices) * 2, GL_UNSIGNED_INT, 0);
        // gl.DrawElements(getObjectDrawMode(object), int32(len(buffers.Element.Indices) * 2), gl.UNSIGNED_INT, gl.PtrOffset(0))

        // Reset buffers
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        // Toggle back from unary context
        popMatrix();

        // Render children (if any?)
        renderObjects(o->objects, program);

        // Toggle back to parent context
        popMatrix();

        i++;
    }
}

Object *loadVoidbox() {

    Object* vb = (Object *) malloc(sizeof(Object));

    strcpy(vb->name, "voidbox");
    strcpy(vb->type, "sphere");
    vb->radius = 9999999999999999999.0f;
    vb->compression = 1.0f;
    vb->inclination = 0.0f;
    vb->revolution  = 0.0f;
    vb->rotation    = -4000.0f;
    vb->distance    = 0.0f;
    vb->cosmic      = true;
    vb->center      = true;
    vb->objects     = NULL;

    return vb;
}

Object *loadStellar() {

    Object* stellar = (Object *) malloc(sizeof(Object));

    // The Sun
    strcpy(stellar->name, "sun");
    strcpy(stellar->type, "sphere");
    stellar->radius = 696300.0f;
    stellar->compression = 1.0f;
    stellar->inclination = 0.0f;
    stellar->tilt        = 0.0f;
    stellar->revolution  = 0.0f;
    stellar->rotation    = 25.38f;
    stellar->distance    = 0.0f;
    stellar->cosmic      = true;
    stellar->center      = true;

    stellar->objects     = (struct ObjectStruct *) calloc(13, sizeof(Object));

    // Mercury
    Object* mercury = &stellar->objects[0];
    strcpy(mercury->name, "mercury");
    strcpy(mercury->type, "sphere");
    mercury->radius = 2439.64f;
    mercury->compression = 1.0f;
    mercury->inclination = 0.0f;
    mercury->tilt        = 7.0f;
    mercury->revolution  = 0.2408467f;
    mercury->rotation    = 58.646225f;
    mercury->distance    = 57909175.0f;

    // Venus
    Object* venus = &stellar->objects[1];
    strcpy(venus->name, "venus");
    strcpy(venus->type, "sphere");
    venus->radius = 6051.59f;
    venus->compression = 1.0f;
    venus->inclination = 40.0f;
    venus->tilt        = 3.39f;
    venus->revolution  = 0.61519726f;
    venus->rotation    = -243.0187f;
    venus->distance    = 108208930.0f;

    // Earth
    Object* earth = &stellar->objects[2];
    strcpy(earth->name, "earth");
    strcpy(earth->type, "sphere");
    earth->radius = 6378.1f;
    earth->compression = 1.0f;
    earth->inclination = 23.45f;
    earth->tilt        = 0.0f;
    earth->revolution  = 1.0000174f;
    earth->rotation    = 0.99726968f;
    earth->distance    = 149597890.0f;
    earth->objects      = (Object *) calloc(1, sizeof(Object));
    // Earth's moon
    Object* moon = &earth->objects[0];
    strcpy(moon->name, "moon");
    strcpy(moon->type, "sphere");
    moon->radius = 1737.1f;
    moon->compression = 1.0f;
    moon->inclination = 1.5424f;
    moon->tilt        = 5.1454f;
    moon->revolution  = 0.07485364384f;
    moon->rotation    = 27.321582f;
    moon->distance    = 384399.0f;

    // Mars
    Object* mars = &stellar->objects[3];
    strcpy(mars->name, "mars");
    strcpy(mars->type, "sphere");
    mars->radius = 3397.00f;
    mars->compression = 1.0f;
    mars->inclination = 25.19f;
    mars->tilt        = 1.85f;
    mars->revolution  = 1.8808476f;
    mars->rotation    = 1.02595675f;
    mars->distance    = 227936640.0f;
    mars->objects       = calloc(2, sizeof(Object));
    // Phobos
    Object* phobos      = &mars->objects[0];
    strcpy(phobos->name, "phobos");
    strcpy(phobos->type, "sphere");
    phobos->radius = 11.2667f;
    phobos->compression = 0.82f;
    phobos->inclination = 26.04f;
    phobos->tilt        = 1.0f;
    phobos->revolution  = 0.0008742465753f;
    phobos->rotation    = 0.31910f;
    phobos->distance    = 9517.58f;
    // Deimos
    Object* deimos      = &mars->objects[1];
    strcpy(deimos->name, "deimos");
    strcpy(deimos->type, "sphere");
    deimos->radius = 6.2f;
    deimos->compression = 0.73f;
    deimos->inclination = 27.58f;
    deimos->tilt        = 2.0f;
    deimos->revolution  = 0.003458739726f;
    deimos->rotation    = 1.26244f;
    deimos->distance    = 23460.0f;

    // Ceres
    Object* ceres = &stellar->objects[4];
    strcpy(ceres->name, "ceres");
    strcpy(ceres->type, "sphere");
    ceres->radius = 473.0f;
    ceres->compression = 1.0f;
    ceres->inclination = 4.0f;
    ceres->tilt        = 10.587f;
    ceres->revolution  = 4.599f;
    ceres->rotation    = 0.3781f;
    ceres->distance    = 413700000.0f;

    // Jupiter
    Object* jupiter = &stellar->objects[5];
    strcpy(jupiter->name, "jupiter");
    strcpy(jupiter->type, "sphere");
    jupiter->radius = 71492.0f;
    jupiter->compression = 1.0f;
    jupiter->inclination = 3.13f;
    jupiter->tilt        = 1.308f;
    jupiter->revolution  = 11.870438356f;
    jupiter->rotation    = 0.41354f;
    jupiter->distance    = 778412010.0f;
    jupiter->objects = (Object *) calloc(4, sizeof(Object));
    // Io moon
    Object* io = &jupiter->objects[0];
    strcpy(io->name, "io");
    strcpy(io->type, "sphere");
    io->radius = 1815.0f;
    io->compression = 1.0f;
    io->inclination = 2.213f;
    io->tilt        = 0.040f;
    io->revolution  = 0.004846953425f;
    io->rotation    = 1.769f;
    io->distance    = 421700.0f;
    // Europa moon
    Object* europa = &jupiter->objects[1];
    strcpy(europa->name, "europa");
    strcpy(europa->type, "sphere");
    europa->radius = 1560.8f;
    europa->compression = 1.0f;
    europa->inclination = 1.791f;
    europa->tilt        = 0.470f;
    europa->revolution  = 0.009703196347f;
    europa->rotation    = 3.551f;
    europa->distance    = 670900.0f;
    // Ganymede moon
    Object* ganymede = &jupiter->objects[2];
    strcpy(ganymede->name, "ganymede");
    strcpy(ganymede->type, "sphere");
    ganymede->radius = 2631.0f;
    ganymede->compression = 1.0f;
    ganymede->inclination = 2.214f;
    ganymede->tilt        = 0.195f;
    ganymede->revolution  = 0.0008167297945f;
    ganymede->rotation    = 7.154553f;
    ganymede->distance    = 1070000.0f;
    // Callisto moon
    Object* callisto = &jupiter->objects[3];
    strcpy(callisto->name, "callisto");
    strcpy(callisto->type, "sphere");
    callisto->radius = 2400.0f;
    callisto->compression = 1.0f;
    callisto->inclination = 2.017f;
    callisto->tilt        = 0.281f;
    callisto->revolution  = 0.001905139269f;
    callisto->rotation    = 16.68902f;
    callisto->distance    = 1883000.0f;
    
    // Saturn
    Object* saturn = &stellar->objects[6];
    strcpy(saturn->name, "saturn");
    strcpy(saturn->type, "sphere");
    saturn->radius = 58232.0f;
    saturn->compression = 1.0f;
    saturn->inclination = 25.33f;
    saturn->tilt        = 2.488f;
    saturn->revolution  = 29.447498f;
    saturn->rotation    = 0.44401f;
    saturn->distance    = 1426725400.0f;
    saturn->objects = (Object *) calloc(9, sizeof(Object));
    // Saturn rings
    Object* rings = &saturn->objects[0];
    strcpy(rings->name, "rings");
    strcpy(rings->type, "circle-filled");
    rings->radius = 138232.0f;
    rings->compression = 1.0f;
    rings->inclination = 25.33f;
    rings->tilt        = 0.0f;
    rings->revolution  = 0.0f;
    rings->rotation    = -8.0f;
    rings->distance    = 65232.0f;
    rings->center       = true;
    // Titan moon
    Object* titan = &saturn->objects[1];
    strcpy(titan->name, "titan");
    strcpy(titan->type, "sphere");
    titan->radius = 2575.0f;
    titan->compression = 1.0f;
    titan->inclination = 0.0f;
    titan->tilt        = 0.33f;
    titan->revolution  = 0.001820253425f;
    titan->rotation    = 15.94542f;
    titan->distance    = 1221850.0f;
    // Enceladus moon
    Object* enceladus = &saturn->objects[2];
    strcpy(enceladus->name, "enceladus");
    strcpy(enceladus->type, "sphere");
    enceladus->radius = 250.0f;
    enceladus->compression = 1.0f;
    enceladus->inclination = 0.0f;
    enceladus->tilt        = 0.02f;
    enceladus->revolution  = 0.0001564175799f;
    enceladus->rotation    = 1.370218f;
    enceladus->distance    = 238020.0f;
    // Iapetus moon
    Object* iapetus = &saturn->objects[3];
    strcpy(iapetus->name, "iapetus");
    strcpy(iapetus->type, "sphere");
    iapetus->radius = 730.0f;
    iapetus->compression = 1.0f;
    iapetus->inclination = 15.47f;
    iapetus->tilt        = 14.72f;
    iapetus->revolution  = 0.009055956621f;
    iapetus->rotation    = 79.33018f;
    iapetus->distance    = 3561300.0f;
    // Mimas
    Object* mimas = &saturn->objects[4];
    strcpy(mimas->name, "mimas");
    strcpy(mimas->type, "sphere");
    mimas->radius = 415.6f;
    mimas->compression = 1.0f;
    mimas->inclination = 0.0f;
    mimas->tilt        = 1.53f;
    mimas->revolution  = 0.002581978082f;
    mimas->rotation    = 0.942422f;
    mimas->distance    = 185520.0f;
    // Hyperion
    Object* hyperion = &saturn->objects[5];
    strcpy(hyperion->name, "hyperion");
    strcpy(hyperion->type, "sphere");
    hyperion->radius = 135.0f;
    hyperion->compression = 1.0f;
    hyperion->inclination = 0.0f;
    hyperion->tilt        = 0.43f;
    hyperion->revolution  = 0.05829208219f;
    hyperion->rotation    = 10.0f;
    hyperion->distance    = 500.0f;
    // Tethys
    Object* tethys = &saturn->objects[6];
    strcpy(tethys->name, "tethys");
    strcpy(tethys->type, "sphere");
    tethys->radius = 531.1f;
    tethys->compression = 1.0f;
    tethys->inclination = 0.0f;
    tethys->tilt        = 1.12f;
    tethys->revolution  = 0.005172060274f;
    tethys->rotation    = 1.887802f;
    tethys->distance    = 295000.0f;
    // Dione
    Object* dione = &saturn->objects[7];
    strcpy(dione->name, "dione");
    strcpy(dione->type, "sphere");
    dione->radius = 560.0f;
    dione->compression = 1.0f;
    dione->inclination = 0.0f;
    dione->tilt        = 0.02f;
    dione->revolution  = 0.00749839726f;
    dione->rotation    = 2.736915f;
    dione->distance    = 377400.0f;    
    // Rhea
    Object* rhea = &saturn->objects[8];
    strcpy(rhea->name, "rhea");
    strcpy(rhea->type, "sphere");
    rhea->radius = 765.0f;
    rhea->compression = 1.0f;
    rhea->inclination = 0.0f;
    rhea->tilt        = 0.35f;
    rhea->revolution  = 0.01237671233f;
    rhea->rotation    = 4.517500f;
    rhea->distance    = 0.0f;
    
    // Uranus
    Object* uranus = &stellar->objects[7];
    strcpy(uranus->name, "uranus");
    strcpy(uranus->type, "sphere");
    uranus->radius = 8557.25f;
    uranus->compression = 1.0f;
    uranus->inclination = 97.86f;
    uranus->tilt        = 0.76f;
    uranus->revolution  = 84.016846f;
    uranus->rotation    = -0.71833f;
    uranus->distance    = 2870972200.0f;
    uranus->objects = (Object *) calloc(6, sizeof(Object));
    // Uranus rings
    Object* uranus_rings = &uranus->objects[0];
    strcpy(uranus_rings->name, "uranus_rings");
    strcpy(uranus_rings->type, "circle-filled");
    uranus_rings->radius = 103000.0f;
    uranus_rings->compression = 1.0f;
    uranus_rings->inclination = 8.0f;
    uranus_rings->tilt        = 0.0f;
    uranus_rings->revolution  = 0.0f;
    uranus_rings->rotation    = 0.0f;
    uranus_rings->distance    = 26840.0f;
    uranus_rings->center      = true;
    uranus_rings->objects = (Object *) calloc(6, sizeof(Object));
    
    // Miranda
    Object* miranda = &uranus->objects[1];
    strcpy(miranda->name, "miranda");
    strcpy(miranda->type, "sphere");
    miranda->radius = 235.8f;
    miranda->compression = 1.0f;
    miranda->inclination = 4.22f;
    miranda->tilt        = 4.22f;
    miranda->revolution  = 0.0001613560502f;
    miranda->rotation    = 1.413479f;
    miranda->distance    = 129780.0f;
    // Ariel
    Object* ariel = &uranus->objects[2];
    strcpy(ariel->name, "ariel");
    strcpy(ariel->type, "sphere");
    ariel->radius = 578.9f;
    ariel->compression = 1.0f;
    ariel->inclination = 0.0f;
    ariel->tilt        = 0.31f;
    ariel->revolution  = 0.006905147945f;
    ariel->rotation    = 2.520379f;
    ariel->distance    = 191240.0f;
    // Oberon
    Object* oberon = &uranus->objects[3];
    strcpy(oberon->name, "oberon");
    strcpy(oberon->type, "sphere");
    oberon->radius = 761.4f;
    oberon->compression = 1.0f;
    oberon->inclination = 0.0f;
    oberon->tilt        = 0.10f;
    oberon->revolution  = 0.03688558904f;
    oberon->rotation    = 13.46324f;
    oberon->distance    = 582600.0f;
    // Umbriel
    Object* umbriel = &uranus->objects[4];
    strcpy(umbriel->name, "umbriel");
    strcpy(umbriel->type, "sphere");
    umbriel->radius = 584.7f;
    umbriel->compression = 1.0f;
    umbriel->inclination = 0.0f;
    umbriel->tilt        = 0.36f;
    umbriel->revolution  = 0.01135390959f;
    umbriel->rotation    = 4.144177f;
    umbriel->distance    = 265970.0f;
    // Titiania
    Object* titania = &uranus->objects[5];
    strcpy(titania->name, "titania");
    strcpy(titania->type, "sphere");
    titania->radius = 788.9f;
    titania->compression = 1.0f;
    titania->inclination = 0.0f;
    titania->tilt        = 0.14f;
    titania->revolution  = 0.02385170411f;
    titania->rotation    = 8.705872f;
    titania->distance    = 435840.0f;
    
    // Neptune
    Object* neptune = &stellar->objects[8];
    strcpy(neptune->name, "neptune");
    strcpy(neptune->type, "sphere");
    neptune->radius = 8766.36f;
    neptune->compression = 1.0f;
    neptune->inclination = 29.56f;
    neptune->tilt        = 1.774f;
    neptune->revolution  = 164.79132f;
    neptune->rotation    = 0.67125f;
    neptune->distance    = 4498252900.0f;
    neptune->objects = (Object *) calloc(1, sizeof(Object));
    // Triton moon
    Object* triton = &neptune->objects[0];
    strcpy(triton->name, "triton");
    strcpy(triton->type, "sphere");
    triton->radius = 1350.0f;
    triton->compression = 1.0f;
    triton->inclination = 129.812f;
    triton->tilt        = 157.35f;
    triton->revolution  = -0.0006708732877f;
    triton->rotation    = -5.87685f;
    triton->distance    = 354800.0f;
    
    // Pluto
    Object* pluto = &stellar->objects[9];
    strcpy(pluto->name, "pluto");
    strcpy(pluto->type, "sphere");
    pluto->radius = 1186.00f;
    pluto->compression = 1.0f;
    pluto->inclination = 122.52f;
    pluto->tilt        = 17.148f;
    pluto->revolution  = 247.92065f;
    pluto->rotation    = -6.38723f;
    pluto->distance    = 5906380000.0f;
    pluto->objects = (Object *) calloc(1, sizeof(Object));
    // Triton moon
    Object* charon = &pluto->objects[0];
    strcpy(charon->name, "charon");
    strcpy(charon->type, "sphere");
    charon->radius = 586.0f;
    charon->compression = 1.0f;
    charon->inclination = 0.001f;
    charon->tilt        = 98.80f;
    charon->revolution  = 0.01749931507f;
    charon->rotation    = 6.38725f;
    charon->distance    = 19640.0f;
    
    // Haumea
    Object* haumea = &stellar->objects[10];
    strcpy(haumea->name, "haumea");
    strcpy(haumea->type, "sphere");
    haumea->radius = 650.0f;
    haumea->compression = 1.0f;
    haumea->inclination = 28.19f;
    haumea->tilt        = 44.0445f;
    haumea->revolution  = 285.4f;
    haumea->rotation    = 0.167f;
    haumea->distance    = 6484000000.0f;

    
    // Makemake
    Object* makemake = &stellar->objects[11];
    strcpy(makemake->name, "makemake");
    strcpy(makemake->type, "sphere");
    makemake->radius = 715.0f;
    makemake->compression = 1.0f;
    makemake->inclination = 0.0f;
    makemake->tilt        = 29.00685f;
    makemake->revolution  = 309.9f;
    makemake->rotation    = 0.2f;
    makemake->distance    = 6850000000.0f;

    
    // Eris
    Object* eris = &stellar->objects[12];
    strcpy(eris->name, "eris");
    strcpy(eris->type, "sphere");
    eris->radius = 1163.0f;
    eris->compression = 1.0f;
    eris->inclination = 44.187f;
    eris->tilt        = 28.19f;
    eris->revolution  = 557.0f;
    eris->rotation    = 0.2f;
    eris->distance    = 10210000000.0f;

    
    return stellar;
}

//
// Created by glagos on 22-07-21.
//

#include "orbit.h"
#include <string.h>

void createOrbitTraces(Object *objects, unsigned int program, unsigned int vao) {
    int i = 0;
    Object* traceObject = (Object*) malloc(sizeof(Object));
    while (objects + i != NULL) {
        traceObject->name           = strcat("orbit-traces-", (objects + i)->name);
        traceObject->type           = "circle";
        traceObject->radius         = (objects + i)->distance;
        traceObject->compression    = 0.0f;
        traceObject->inclination    = 0.0f;
        traceObject->tilt           = (objects + i)->tilt;
        traceObject->revolution     = 0.0f;
        traceObject->rotation       = 0.0f;
        traceObject->distance       = 0.0f;
        traceObject->center         = true;
        traceObject->radiate        = false;
        traceObject->cosmic         = true;
        traceObject->objects        = (struct Object *) malloc(sizeof(Object));

        objects[i] = *traceObject;

        // Append orbit traces for child objects
        createOrbitTraces((Object *) objects[i].objects, program, vao);

        i++;
    }
}

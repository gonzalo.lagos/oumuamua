//
// Created by glagos on 21-07-21.
//

#include "camera.h"
#include "config.h"
#include "event.h"
#include "speed.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

float getEyeX(CameraData *cameraData) {
    return cameraData->positionEye[0];
}

float getEyeY(CameraData *cameraData) {
    return cameraData->positionEye[1];
}

float getEyeZ(CameraData *cameraData) {
    return cameraData->positionEye[2];
}

float getTargetX(CameraData *cameraData) {
    return cameraData->positionTarget[0];
}

float getTargetY(CameraData *cameraData) {
    return cameraData->positionTarget[1];
}

float getTargetZ(CameraData *cameraData) {
    return cameraData->positionTarget[2];
}

void moveEyeX(CameraData *cameraData, float increment) {
    cameraData->positionEye[0] += increment;
}

void moveEyeY(CameraData *cameraData, float increment) {
    cameraData->positionEye[1] += increment;
}

void moveEyeZ(CameraData *cameraData, float increment) {
    cameraData->positionEye[2] += increment;
}

void moveTargetX(CameraData *cameraData, float increment) {
    cameraData->positionTarget[0] += increment;
}

void moveTargetY(CameraData *cameraData, float increment) {
    cameraData->positionTarget[1] += increment;
}

void moveTargetZ(CameraData *cameraData, float increment) {
    cameraData->positionTarget[2] += increment;
}

void defaultEye(CameraData *cameraData) {
    glm_vec3_copy(ConfigCameraDefaultEye, cameraData->positionEye);
}

void defaultTarget(CameraData *cameraData) {
    glm_vec3_copy(ConfigCameraDefaultTarget, cameraData->positionTarget);
}

void defaultInertia(CameraData *cameraData) {
    cameraData->inertiaDrag = 0.0;
    cameraData->inertiaTurn = 0.0;
}

CameraData *getCamera() {
    return &InstanceCamera;
}

void processEventCameraEye() {
    double celerity;
    double rotationX;
    double rotationY;
    double inertiaDrag;
    double inertiaTurn;

    CameraData* camera      = getCamera();
    EventKeyState* keyState = getEventKeyState();
    float timeFactor        = normalizedTimeFactor();

    // Decrease speed if diagonal move
    if (keyState->moveTurbo) {
        celerity = ConfigCameraMoveCelerityTurbo;
    } else {
        celerity = ConfigCameraMoveCelerityCruise;
    }

    if ((keyState->moveUp || keyState->moveDown)
            && (keyState->moveLeft || keyState->moveRight)) {
        celerity /= sqrt(2.0);
    }

    // Acquire rotation around axis
    rotationX = (double) getTargetX(camera);
    rotationY = (double) getTargetY(camera);

    // Process camera move position (keyboard)
    if (keyState->moveUp)
        produceInertia(&camera->inertiaDrag, ConfigCameraInertiaProduceForward, celerity);
    if (keyState->moveDown)
        produceInertia(&camera->inertiaDrag, ConfigCameraInertiaProduceBackward, celerity);
    if (keyState->moveLeft)
        produceInertia(&camera->inertiaTurn, ConfigCameraInertiaProduceForward, celerity);
    if (keyState->moveRight)
        produceInertia(&camera->inertiaTurn, ConfigCameraInertiaProduceBackward, celerity);

    // Apply new position with intertia
    inertiaDrag = consumeInertia(&camera->inertiaDrag);
    inertiaTurn = consumeInertia(&camera->inertiaTurn);

    moveEyeX(camera, timeFactor * (float)(inertiaDrag * -1.0 * sin(rotationY)) + (float)(inertiaTurn * cos(rotationY)));
    moveEyeZ(camera, timeFactor * (float)(inertiaDrag * cos(rotationY)) + (float)(inertiaTurn * sin(rotationY)));
    moveEyeY(camera, timeFactor * (float)(inertiaDrag * sin(rotationX)));

    // Translation: walk
    // Translate3D()
    mat4 matrix;
    glm_mat4_identity(matrix);
    matrix[0][3] = getEyeX(camera);
    matrix[1][3] = getEyeY(camera);
    matrix[2][3] = getEyeZ(camera);
    glm_mat4_mul(camera->camera, matrix, camera->camera);
}

void processEventCameraTarget() {
    CameraData* camera      = getCamera();
    EventKeyState* keyState = getEventKeyState();
    float timeFactor        = normalizedTimeFactor();

    moveTargetX(camera, timeFactor * keyState->watchY *
                                (float)M_PI * (float)ConfigCameraTargetAmortizeFactor);
    moveTargetY(camera, timeFactor * keyState->watchX *
                                (float)M_PI * 2 * (float)ConfigCameraTargetAmortizeFactor);

    // Rotation: view

    // X axis
    mat4 mx;
    glm_mat4_zero(mx);
    vec3 vx = { 1.0f, 0.0f, 0.0f };
    float tx = getTargetX(camera);

    glm_rotate(mx, tx, vx);
    glm_mat4_mul(camera->camera, mx, camera->camera);

    // Y axis
    mat4 my;
    glm_mat4_zero(my);
    vec3 vy = { 1.0f, 0.0f, 0.0f };
    float ty = getTargetY(camera);

    glm_rotate(my, ty, vy);
    glm_mat4_mul(camera->camera, my, camera->camera);

    // Z axis
    mat4 mz;
    glm_mat4_zero(mz);
    vec3 vz = { 1.0f, 0.0f, 0.0f };
    float tz = getTargetZ(camera);

    glm_rotate(mz, tz, vz);
    glm_mat4_mul(camera->camera, mz, camera->camera);
}

void updateCamera() {
    CameraData* camera = getCamera();

    mat4 identityMatrix;
    glm_mat4_identity(identityMatrix);
    glm_mat4_copy(identityMatrix, camera->camera);

    processEventCameraTarget();
    processEventCameraEye();
}

void resetCamera() {
    CameraData* camera = getCamera();
    // Reset camera modifiers
    resetMouseCursor();
    // Reset camera itself
    defaultInertia(camera);
    defaultEye(camera);
    defaultTarget(camera);
}

void bindCamera() {
    CameraData* camera = getCamera();
    glUniformMatrix4fv(camera->cameraUniform, 1, false, camera->camera[0]);
}

void createCamera(unsigned int program) {
    CameraData* camera = getCamera();
    camera->cameraUniform = glGetUniformLocation(program, "cameraUniform\x00");
    // Default inertia (none)
    defaultInertia(camera);
    // Default camera position
    defaultEye(camera);
    defaultTarget(camera);
}

void produceInertia(double *inertia, double increment, double celerity) {
    *inertia = increment * celerity;
    // Cap inertia to maximum value
    if (*inertia > celerity) {
        *inertia = celerity;
    } else if (*inertia < -1.0 * celerity) {
        *inertia = -1.0 * celerity;
    }
}

double consumeInertia(double *inertia) {
    if (*inertia > 0.0) {
        *inertia += ConfigCameraInertiaConsumeForward;
    } else if (*inertia < 0.0) {
        *inertia += ConfigCameraInertiaProduceBackward;
    }

    return *inertia;
}

![](https://img.shields.io/badge/vulkan-%23AC162C.svg?&style=for-the-badge&logo=vulkan&logoColor=white) ![](https://img.shields.io/badge/go-%2300ADD8.svg?&style=for-the-badge&logo=go&logoColor=white) ![](https://img.shields.io/badge/opengl-%235586A4.svg?&style=for-the-badge&logo=opengl&logoColor=white) ![](https://img.shields.io/badge/c-%23A8B9CC.svg?&style=for-the-badge&logo=c&logoColor=black) ![](https://img.shields.io/badge/opencv-%235C3EE8.svg?&style=for-the-badge&logo=opencv&logoColor=white) ![](https://img.shields.io/badge/gnome-%234A86CF.svg?&style=for-the-badge&logo=gnome&logoColor=white)

# Oumuamua

#### Based on *@Callisto* project, it aims to be more than a port. Using the **GObject** type system on **C**, it tries to remain pure as possible and perform elegantly.

> ##### This project knows no horizon, likewise the universe expands faster than light.


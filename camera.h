//
// Created by glagos on 21-07-21.
//

#ifndef OMUAMUA_CAMERA_H
#define OMUAMUA_CAMERA_H

#include <cglm/cglm.h>

typedef struct CameraDataStruct {
    mat4    camera;
    int     cameraUniform;

    vec3    positionEye;
    vec3    positionTarget;

    double  inertiaDrag;
    double  inertiaTurn;
} CameraData;

// InstanceCamera Stores camera state
static CameraData InstanceCamera;

float getEyeX(CameraData* cameraData);
float getEyeY(CameraData* cameraData);
float getEyeZ(CameraData* cameraData);

float getTargetX(CameraData *cameraData);
float getTargetY(CameraData *cameraData);
float getTargetZ(CameraData *cameraData);

void moveEyeX(CameraData* cameraData, float increment);
void moveEyeY(CameraData* cameraData, float increment);
void moveEyeZ(CameraData* cameraData, float increment);

void moveTargetX(CameraData* cameraData, float increment);
void moveTargetY(CameraData* cameraData, float increment);
void moveTargetZ(CameraData* cameraData, float increment);

void defaultEye(CameraData* cameraData);
void defaultTarget(CameraData* cameraData);
void defaultInertia(CameraData* cameraData);

CameraData* getCamera();
void createCamera(unsigned int program);

void produceInertia(double* inertia, double increment, double celerity);
double consumeInertia(double* inertia);

void processEventCameraEye();
void processEventCameraTarget();
void updateCamera();
void resetCamera();
void bindCamera();


#endif //OMUAMUA_CAMERA_H

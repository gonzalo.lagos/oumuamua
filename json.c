//
// Created by glagos on 27-07-21.
//

#include <string.h>
#include "json.h"

void parse_object(Object* object, const char* key, json_t* element) {


    if (strcmp(key, "name") == 0) {
        parse_json_string(element, object->name);
    } else if (strcmp(key, "type") == 0) {
        parse_json_string(element, object->type);
    } else if (strcmp(key, "radius") == 0) {
        parse_json_float(element, &object->radius);
    } else if (strcmp(key, "compression") == 0) {
        parse_json_float(element, &object->compression);
    } else if (strcmp(key, "inclination" ) == 0) {
        parse_json_float(element, &object->inclination);
    } else if (strcmp(key, "tilt") == 0) {
        parse_json_float(element, &object->tilt);
    } else if (strcmp(key, "revolution") == 0) {
        parse_json_float(element, &object->revolution);
    } else if (strcmp(key, "rotation") == 0) {
        parse_json_float(element, &object->rotation);
    } else if (strcmp(key, "distance") == 0) {
        parse_json_float(element, &object->distance);
    } else if (strcmp(key, "radiate") == 0) {
        parse_json_boolean(element, &object->radiate);
    } else if (strcmp(key, "center") == 0) {
        parse_json_boolean(element, &object->center);
    } else if (strcmp(key, "objects") == 0) {
        print_json(element, (Object *) object->objects);
    }
}


void print_json(json_t *root, Object* object) { print_json_aux(root, object, NULL); }

void print_json_aux(json_t *element, Object* object, const char* key) {
    switch (json_typeof(element)) {
        case JSON_OBJECT:
            print_json_object(element, object);
            break;
        case JSON_ARRAY:
            print_json_array(element, object);
            break;
        case JSON_STRING:
            print_json_string(element);
            parse_object(object, key, element);
            break;
        case JSON_INTEGER:
            print_json_integer(element);
            parse_object(object, key, element);
            break;
        case JSON_REAL:
            print_json_real(element);
            parse_object(object, key, element);
            break;
        case JSON_TRUE:
            print_json_true(element);
            parse_object(object, key, element);
            break;
        case JSON_FALSE:
            print_json_false(element);
            parse_object(object, key, element);
            break;
        case JSON_NULL:
            print_json_null(element);
            break;
        default:
            fprintf(stderr, "unrecognized JSON type %d\n", json_typeof(element));
    }
}


const char *json_plural(size_t count) { return count == 1 ? "" : "s"; }

void print_json_object(json_t *element, Object* object) {

    size_t size;
    const char *key;
    json_t *value;

    // print_json_();

    size = json_object_size(element);
    /*
    if (object == NULL) {
        object = (Object *) calloc(size, sizeof(Object));
    }
    */


    printf("JSON Object of %lld pair%s:\n", (long long)size, json_plural(size));
    json_object_foreach(element, key, value) {
        // print_json_( + 2);
        printf("JSON Key: \"%s\"\n", key);
        print_json_aux(value + 2, object, key);
        parse_object(object, key, element);
    }
}

void print_json_objects(json_t *element, Object *object, size_t len) {

    size_t size;
    const char *key;
    json_t *value;

    // print_json_();
    size = json_object_size(element);

    printf("JSON Object of %lld pair%s:\n", (long long)size, json_plural(size));
    for (int i = 0; i < len; i++) {
        json_object_foreach(element, key, value) {
            printf("JSON Key: \"%s\"\n", key);
            parse_object(object, key, element);
            print_json_aux(value + 2, object, key);
        }
    }
}

void print_json_array(json_t *element, Object* object) {
    size_t i;
    size_t size = json_array_size(element);
    // print_json_();

    if (object == NULL) {
        if (size == 1) {
            // Main object
            object = (Object *) malloc(sizeof(Object));
            fprintf(stdout, "Main object allocated.\n");
        } else {
            // Inner objects
            object = (Object *) calloc(size, sizeof(Object));
            fprintf(stdout, "Inner objects allocated.\n");
        }
    }


    // print_json_objects(element, (Object *) object->objects, size);

    printf("JSON Array of %lld element%s:\n", (long long)size, json_plural(size));
    for (i = 0; i < size; i++) {
        print_json_aux(json_array_get(element, i) + 2, object, NULL);
    }
}

void print_json_string(json_t *element) {
    printf("JSON String: \"%s\"\n", json_string_value(element));
}

void print_json_integer(json_t *element) {

    printf("JSON Integer: \"%" JSON_INTEGER_FORMAT "\"\n", json_integer_value(element));
}

void print_json_real(json_t *element) {

    printf("JSON Real: %f\n", json_real_value(element));
}

void print_json_true(json_t *element) {
    (void)element;
    printf("JSON True\n");
}

void print_json_false(json_t *element) {
    (void)element;
    printf("JSON False\n");
}

void print_json_null(json_t *element) {
    (void)element;
    printf("JSON Null\n");
}

/** ---------------------------------------------- **/

void parse_json_string(json_t *element, char *string) {
    const char* string_element = json_string_value(element);
    size_t len = strlen(string_element);
    string = (char *) malloc(sizeof(string) * len);
    strcpy(string, string_element);
}

void parse_json_integer(json_t *element, int *integer) {
    int int_value = (int) json_integer_value(element);
    *integer = int_value;
}

void parse_json_real(json_t* element, double* real) {
    double double_value = json_real_value(element);
    *real = double_value;
}

void parse_json_float(json_t *element, float *fp) {
    float float_value = (float) json_real_value(element);
    *fp = float_value;
}

void parse_json_boolean(json_t *element, bool *boolean) {
    bool bool_value;
    switch (json_typeof(element)) {
        case JSON_TRUE: bool_value = true;
                        break;
        case JSON_FALSE: bool_value = false;
                        break;
    }
    *boolean = bool_value;
}

void parse_json_root_array(json_t *element) {

}



/** ----------------------------------------------- **/

/*
 * Parse text into a JSON object. If text is valid JSON, returns a
 * json_t structure, otherwise prints and error and returns null.
 */
json_t *load_json(const char *text) {
    json_t *root;
    json_error_t error;

    root = json_loads(text, 0, &error);

    if (root) {
        return root;
    } else {
        fprintf(stderr, "json error on line %d: %s\n", error.line, error.text);
        return (json_t *)0;
    }
}

/*
 * Print a prompt and return (by reference) a null-terminated line of
 * text.  Returns NULL on eof or some error.
 */
char *read_line(char *line, int max_chars) {
    printf("Type some JSON > ");
    fflush(stdout);
    return fgets(line, max_chars, stdin);
}

/* ================================================================
 * main
 */

#define MAX_CHARS 4096



#ifndef OMUAMUA_OBJECT_ELEMENT_H
#define OMUAMUA_OBJECT_ELEMENT_H

#include <stdbool.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define OUMUAMUA_TYPE_OBJECT_ELEMENT (oumuamua_object_element_get_type ())
G_DECLARE_FINAL_TYPE (OumuamuaObjectElement, oumuamua_object_element, OUMUAMUA, OBJECT_ELEMENT, GObject)


OumuamuaObjectElement *
oumuamua_object_element_new (void);


G_END_DECLS

#endif


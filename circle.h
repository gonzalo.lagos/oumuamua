//
// Created by glagos on 15-07-21.
//

#ifndef OMUAMUA_CIRCLE_H
#define OMUAMUA_CIRCLE_H


#include "config.h"
#include "object.h"



// Circle  Maps a circle object
typedef ObjectElement Circle;

Circle generateCircleFilledFromObject(Object* object);
Circle generateCircleFromObject(Object* object);
Circle generateCircle(float radius, float thickness, bool radiate);
void generateCircleObject(Circle* circle, float radiusN, float thickness,
                          double angle, int angleMax, float normalDirection,
                          int nbVertices, int passIndex,
                          int* i, int* j, int* k, int* l);


#endif //OMUAMUA_CIRCLE_H

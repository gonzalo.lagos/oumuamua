//
// Created by glagos on 14-07-21.
//

#include "window.h"



struct WindowData* getWindowData() {
    return &InstanceWindowData;
}

void initializeWindow(GLFWmonitor *monitor) {
    struct WindowData* windowData = getWindowData();
    const GLFWvidmode* videoMode = glfwGetVideoMode(monitor);
    // Initialize window size
    windowData->width = videoMode->width;
    windowData->height = videoMode->height;
    // Lock window framerate to monitor framerate
    setFramerate(getSpeed(), videoMode->refreshRate);
}

void adjustWindow(GLFWwindow *window) {
    int fbWidth, fbHeight;
    glfwGetFramebufferSize(window, &fbWidth, &fbHeight);
    handleAdjustWindow(window, fbWidth, fbHeight);
}

void handleAdjustWindow(GLFWwindow *window, int width, int height) {
    struct WindowData* windowData = getWindowData();
    // Adjust window size
    windowData->width = width;
    windowData->height = height;
}





#include <stdio.h>
#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "window.h"
#include "event.h"
#include "projection.h"
#include "camera.h"
#include "buffer.h"
#include "shader.h"
#include "matrix.h"
#include "program.h"
#include "orbit.h"
#include "object.h"
#include "util.h"


int main() {

    GLFWwindow* window = 0;
    GLFWmonitor* monitor = 0;
    unsigned int program;
    unsigned int vao;
    Object *voidbox = 0, *stellar = 0;
    int error;

    // Create window
    glewExperimental = true;
    error = glfwInit();
    if (error == GLFW_FALSE) {
        fprintf(stderr, "Failed to initialize GLFW.\nExiting now...\n");
        exit(-1);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

    monitor = glfwGetPrimaryMonitor();
    initializeWindow(monitor);

    int window_width = getWindowData()->width;
    int window_height = getWindowData()->height;
    window = glfwCreateWindow(window_width, window_height,
                     ConfigWindowTitle, NULL, NULL);
    /*
    if (window == NULL) {
        fprintf(stderr, "Failed to create GLFW window.\nExiting now...\n");
        exit(-1);
    }
     */

    // Fullscreen window ?
    if (ConfigWindowFullScreen == true) {
        // Adjust window to fullscreen mode, once we got the screen DPI (Retina screens)
        adjustWindow(window);
        // Recreate window to match fullscreen size with good framebuffer size (keeps high-DPI resolution)
        glfwSetFramebufferSizeCallback(window, handleAdjustWindow);
        glfwDestroyWindow(window);

        window = glfwCreateWindow(getWindowData()->width, getWindowData()->height,
                                  ConfigWindowTitle, monitor, NULL);

        if (window == NULL) {
            fprintf(stderr, "Failed to create fullscreen GLFW window.\nExiting now...\n");
            exit(-1);
        }
    }

    // Bind window context
    glfwMakeContextCurrent(window);
    // Bind key listeners
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

    glfwSetKeyCallback(window, handleKey);
    glfwSetCursorPosCallback(window, handleMouseCursor);
    glfwSetScrollCallback(window, handleMouseScroll);

    // Initialize OpenGL

    // Configure the shaders program
    // Read vertex and fragment shader files
    char* vs = textFileRead(VertexShaderFilename);
    char* fs = textFileRead(FragmentShaderFilename);
    const char* vertexSource = vs;
    const char* fragmentSource = vs;

    program = newProgram(vertexSource, fragmentSource);
    glUseProgram(program);

    // Create environment
    createProjection(program);
    createCamera(program);

    // Create the VAO (Vertex Array Objects)
    // Notice: this stores links between attributes and active vertex data
    glGenVertexArrays(1, &vao);

    // Load the map of stellar objects + voidbox (aka skybox)
    voidbox = loadObjects("voidbox");
    stellar = loadObjects("stellar");

    // Apply orbit traces
    createOrbitTraces(stellar, program, vao);

    // Create all object buffers
    createAllBuffers(voidbox, program, vao);
    createAllBuffers(stellar, program, vao);

    // Initialize shaders
    initializeShaders(program);

    // Configure global settings
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LESS);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // Render loop
    while (!glfwWindowShouldClose(window)) {

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Global routines
        updateElapsedTime(glfwGetTime());
        glUseProgram(program);

        // Initialize stack matrix
        initializeMatrix();

        // Update context
        updateCamera();

        // Bind context
        bindProjection();
        bindCamera();

        // Render skybox
        glDisable(GL_DEPTH_TEST);
        renderObjects(voidbox, program);
        glEnable(GL_DEPTH_TEST);

        // Render all stellar objects in the map
        renderObjects(stellar, program);

        glfwPollEvents();
        glfwSwapBuffers(window);

        deferSceneUpdate();
    }

    glfwTerminate();

    return 0;
}

//
// Created by glagos on 14-07-21.
//

#include "event.h"

#include "speed.h"
#include "window.h"
#include "camera.h"


EventKeyState *getEventKeyState() {
    return &InstanceEventKeyState;
}

void handleKey(GLFWwindow *window, int key, int scancode, int action, int mods) {
    EventKeyState* keyState = getEventKeyState();

    // Main controls
    if (key == GLFW_KEY_ESCAPE) {
        if (action == GLFW_PRESS) {
            glfwSetWindowShouldClose(window, true);
        }
    }

    // Camera control keys
    if (key == GLFW_KEY_R) {
        // Release?
        if (action == GLFW_RELEASE) {
            // Immediately reset camera
            resetCamera();
        }
    }

    // Camera controls
    if (key == GLFW_KEY_SPACE) {
        // Press or Release ?
        if (action == GLFW_PRESS) {
            keyState->moveTurbo = true;
        } else if (action == GLFW_RELEASE) {
            keyState->moveTurbo = false;
        }
    }

    if (key == GLFW_KEY_UP) {
        if (action == GLFW_PRESS) {
            keyState->moveUp = true;
            keyState->moveDown = false;
        } else if (action == GLFW_RELEASE) {
            keyState->moveUp = false;
        }
    }

    if (key == GLFW_KEY_DOWN) {
        if (action == GLFW_PRESS) {
            keyState->moveUp = false;
            keyState->moveDown = true;
        } else if (action == GLFW_RELEASE) {
            keyState->moveDown = false;
        }
    }

    if (key == GLFW_KEY_LEFT) {
        if (action == GLFW_PRESS) {
            keyState->moveLeft = true;
            keyState->moveRight = false;
        } else if (action == GLFW_RELEASE) {
            keyState->moveLeft = true;
        }
    }

    if (key == GLFW_KEY_RIGHT) {
        if (action == GLFW_PRESS) {
            keyState->moveLeft = false;
            keyState->moveRight = true;
        } else if (action == GLFW_RELEASE) {
            keyState->moveRight = false;
        }
    }

}

void handleMouseCursor(GLFWwindow *window, double positionX, double positionY) {
    EventKeyState* keyState = getEventKeyState();
    struct WindowData* windowData = getWindowData();

    // Bind new watch position
    if (positionX >= 0 && positionX <= (double)windowData->width) {
        keyState->watchX = (float)positionX * (1.0f / (float)windowData->width) - 0.5f;
    }

    if (positionY >= 0 && positionY <= (double)windowData->height) {
        keyState->watchY = (float)positionY * (1.0f / (float)windowData->height) - 0.5f;
    }
}

void handleMouseScroll(GLFWwindow *window, double offsetX, double offsetY) {
    // Update scene simulation speed
    updateSpeedFactor(offsetY);
}

void resetMouseCursor() {
    EventKeyState* keyState = getEventKeyState();
    keyState->watchX = 0.0f;
    keyState->watchY = 0.0f;
}



//
// Created by glagos on 22-07-21.
//

#include "projection.h"


ProjectionData *getProjection() {
    return &InstanceProjection;
}

void createProjection(unsigned int program) {
    ProjectionData* projection  = getProjection();
    struct WindowData* window   = getWindowData();

    // Perspective (projection)
    glm_perspective(glm_rad(45.0f),
                    (float)(window->width) / (float)(window->height),
                    (float)ConfigProjectionFieldNear,
                    (float)ConfigProjectionFieldFar,
                    projection->projection);

    // Projection uniform
    glGetUniformLocation(program, "projectionUniform\x00");
}

void bindProjection() {
    ProjectionData* projection = getProjection();
    glUniformMatrix4fv(projection->projectionUniform, 1, false, projection->projection[0]);
}



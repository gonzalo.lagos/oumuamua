//
// Created by glagos on 20-07-21.
//

#ifndef OMUAMUA_MATRIX_H
#define OMUAMUA_MATRIX_H

#include <cglm/cglm.h>

static int MatrixStackLimit = 100000;

// MatrixStack  Maps the stack of rendering matrices
typedef struct MatrixStackStruct {
    mat4*                       current;
    struct MatrixStackStruct*   stack;
} MatrixStack;

// MatrixUniforms  Maps uniform matrices
typedef struct MatrixUniformsStruct {
    int model;
    int normal;
    int texture;
} MatrixUniforms;


// InstanceMatrixStack  Stores the stack of rendering matrixes
static MatrixStack InstanceMatrixStack;

// InstanceMatrixUniforms  Stores uniform matrixes
static MatrixUniforms InstanceMatrixUniforms;


void initializeMatrix();
mat4* getMatrix();
void setMatrix(mat4* matrix);

MatrixStack* getMatrixStack();
void setMatrixStack(MatrixStack* matrixStack);
void pushMatrix();
void popMatrix();

MatrixUniforms* getMatrixUniforms();
void setMatrixUniforms(unsigned int program);



#endif //OMUAMUA_MATRIX_H

//
// Created by glagos on 23-07-21.
//

#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include "util.h"

char *stringRepeat(char *s, int ntimes) {
    char* string;
    int defaultLength = 100;
    if (strlen(s) + ntimes * strlen(s) > defaultLength) {
        defaultLength *= 2;
    }

    string = (char*) malloc(sizeof(char) * defaultLength);
    for (int i = 0; i < ntimes; i++) {
        strcat(string, s);
    }

    return string;
}

char *ltrim(char *s)
{
    while(isspace(*s)) s++;
    return s;
}

char *rtrim(char *s)
{
    char* back;
    int len = strlen(s);

    if(len == 0)
        return(s);

    back = s + len;
    while(isspace(*--back));
    *(back+1) = '\0';
    return s;
}

char *trim(char *s)
{
    return rtrim(ltrim(s));
}

// HomogRotate3D creates a 3D rotation Matrix that rotates by (radian) angle about some arbitrary axis given by a normalized Vector.
// It produces a homogeneous matrix (4x4)
//
// Where c is cos(angle) and s is sin(angle), and x, y, and z are the first, second, and third elements of the axis vector (respectively):
//
//    [[ x^2(1-c)+c, xy(1-c)-zs, xz(1-c)+ys, 0 ]]
//    [[ xy(1-c)+zs, y^2(1-c)+c, yz(1-c)-xs, 0 ]]
//    [[ xz(1-c)-ys, yz(1-c)+xs, z^2(1-c)+c, 0 ]]
//    [[ 0         , 0         , 0         , 1 ]]
void HomogRotate3D(float angle, vec3 *axis, mat4* matrix) {
    float x = *axis[0];
    float y = *axis[1];
    float z = *axis[2];

    float s = sinf(angle);
    float c = cosf(angle);

    float k = 1 - c;

    mat4 m = { x*x*k + c, x*y*k + z*s, x*z*k - y*s, 0,
                    x*y*k - z*s, y*y*k + c, y*z*k + x*s, 0,
                    x*z*k + y*s, y*z*k - x*s, z*z*k + c, 0,
                    0, 0, 0, 1};

    glm_mat4_copy(m, *matrix);
}

// Mat4Normal calculates the Normal of the Matrix (aka the inverse transpose)
void Mat4Normal(vec4 *m, vec3 *dest) {
    mat4 inv = GLM_MAT4_ZERO_INIT;  // Inverted
    mat4 t = GLM_MAT4_ZERO_INIT;    // Transposed
    mat3 normal;

    glm_mat4_inv(m, inv);
    glm_mat4_transpose_to(inv, t);

    glm_vec3_zero(*normal);

    normal[0][0] = t[0][0];
    normal[0][1] = t[0][1];
    normal[0][2] = t[0][2];
    normal[1][0] = t[1][0];
    normal[1][1] = t[1][1];
    normal[1][2] = t[1][2];
    normal[2][0] = t[2][0];
    normal[2][1] = t[2][1];
    normal[2][2] = t[2][2];

    glm_vec3_copy(*normal, *dest);
}

void Translate3D(float tx, float ty, float tz, vec4 *m) {
    m[0][0] = 1;
    m[0][1] = 0;
    m[0][2] = 0;
    m[0][3] = 0;
    m[1][0] = 0;
    m[1][1] = 1;
    m[1][2] = 0;
    m[1][3] = 0;
    m[2][0] = 0;
    m[2][1] = 0;
    m[2][2] = 1;
    m[2][3] = 0;
    m[3][0] = tx;
    m[3][1] = ty;
    m[3][2] = tz;
    m[3][3] = 1;
}

int lenIntArray(int *array) {
    if (array == NULL)
        return -1;

    int i = 0;
    while( (array + i) != NULL) {
        i++;
    }

    return i;
}

char *textFileRead(char *fn) {
    FILE *fp;
    char *content = NULL;

    int count = 0;

    if (fn != NULL) {
        fp = fopen(fn,"rt");

        if (fp != NULL) {

            fseek(fp, 0, SEEK_END);
            count = ftell(fp);
            rewind(fp);

            if (count > 0) {
                content = (char *)malloc(sizeof(char) * (count+1));
                count = fread(content,sizeof(char),count,fp);
                content[count] = '\0';
            }
            fclose(fp);
        }
    }
    return content;
}

void init_string(char *string, char *content) {
    if (content == NULL) {
        fprintf(stderr, "Initializing string with NULL content.\nExiting...\n");
        exit(EXIT_FAILURE);
    }

    size_t len = strlen(content);
    string = (char *) malloc(sizeof(char) * len + 1);
    strcpy(string, content);
}

void init_string_size(char *string, char *content, int len) {
    if (content == NULL) {
        fprintf(stderr, "Initializing string with NULL content.\nExiting...\n");
        exit(EXIT_FAILURE);
    }

    size_t l = strlen(content);
    string = (char *) malloc(sizeof(char) * l + len + 1);
    strcpy(string, content);
}


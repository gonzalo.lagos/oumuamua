//
// Created by glagos on 14-07-21.
//

#ifndef OMUAMUA_SPEED_H
#define OMUAMUA_SPEED_H

#include "config.h"
#include "object.h"

struct Speed {
    double  timePrevious;
    double  timeElapsed;
    double  factor;
    double  framerate;
};


// InstanceSpeed  Stores current scene speed
static struct Speed InstanceSpeed = {
        0.0,
        0.0,
        1.0,
        ConfigSpeedFramerateDefault
};


void setFramerate(struct Speed* speed, int framerate);
struct Speed* getSpeed();
void updateSpeedFactor(double factorOffset);
void updateElapsedTime(double nowTime);
void deferSceneUpdate();

float angleSince(float angleTime, double factor, double elapsed);
float revolutionAngleSince(Object* object, double factor, double elapsed);
float rotationAngleSince(Object* object, double factor, double elapsed);
float revolutionAngleSinceLast(Object* object);
float rotationAngleSinceLast(Object* object);
float revolutionAngleSinceStart(Object* object);
float rotationAngleSinceStart(Object* object);

float normalizedTimeFactor();

#endif //OMUAMUA_SPEED_H

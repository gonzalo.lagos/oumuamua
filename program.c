//
// Created by glagos on 23-07-21.
//

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "program.h"
#include "util.h"

unsigned int newProgram(const char* vertexSource, const char* fragmentSource) {
    int status;
    int logLength;
    char* log;

    // Read shader files.

    unsigned int vertexShader = compileShader(vertexSource, GL_VERTEX_SHADER);
    unsigned int fragmentShader = compileShader(fragmentSource, GL_FRAGMENT_SHADER);

    unsigned int program = glCreateProgram();

    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status == GL_FALSE) {
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
        log = stringRepeat("\x00", logLength + 1);
        glGetProgramInfoLog(program, logLength, NULL, log);

        fprintf(stderr, "failed to link program: %s\n", log);
        exit(EXIT_FAILURE);
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return program;
}

unsigned int compileShader(const char* source, unsigned int shaderType) {
    int status;
    int logLength;
    char* log;

    unsigned int shader = glCreateShader(shaderType);

    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE) {
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        log = stringRepeat("\x00", logLength + 1);
        glGetShaderInfoLog(shader, logLength, NULL, log);

        fprintf(stderr, "Failed to compile %s: %s\n", source, log);
        exit(EXIT_FAILURE);
    }

    return shader;
}



//
// Created by glagos on 14-07-21.
//

#ifndef OMUAMUA_CONFIG_H
#define OMUAMUA_CONFIG_H


//
// Created by glagos on 14-07-21.
//

#include <math.h>
#include <cglm/vec3.h>


/* --------------- Window ------------------------------*/
static  const char* ConfigWindowTitle = "Callisto - Solar System Simulator";
static bool ConfigWindowFullScreen = false;
/* ----------------------------------------------------- */

/* --------------- Speed ------------------------------*/
static const double ConfigSpeedFramerateDefault = 60.0;
static const double ConfigSpeedFramerateFactor = 1.5;
/* ----------------------------------------------------- */

/* --------------- Projection ------------------------------*/
static double ConfigProjectionFieldNear = 0.1;
static double ConfigProjectionFieldFar = 9999999999999999999.0;
/* ----------------------------------------------------- */

/* --------------- Time  ------------------------------*/
static const long ConfigTimeSecondToMilliseconds = 1000;
static const long ConfigTimeMinuteToMilliseconds = 60 * ConfigTimeSecondToMilliseconds;
static const long ConfigTimeHourToMilliseconds = 60 * ConfigTimeMinuteToMilliseconds;
static const long ConfigTimeDayToMilliseconds = 24 * ConfigTimeHourToMilliseconds;
static const long ConfigTimeYearToMilliseconds = 365 * ConfigTimeDayToMilliseconds;
static long ConfigTimeStartFromMilliseconds = ConfigTimeYearToMilliseconds;
static float ConfigTimeNormalizeFactor = 64.0f;
/* ----------------------------------------------------- */

/* --------------- Camera ------------------------------*/
static double ConfigMathDegreeToRadian = M_PI / 180;
/* ----------------------------------------------------- */

/* --------------- Camera ------------------------------*/
static vec3 ConfigCameraDefaultEye = {300, -350, -800};
static vec3 ConfigCameraDefaultTarget = {0.255f, 0.650f, 0.000f};
static double ConfigCameraMoveCelerityCruise = 1.0;
static double ConfigCameraMoveCelerityTurbo = 10.0;
static double ConfigCameraInertiaProduceForward = 0.0075;
static double ConfigCameraInertiaProduceBackward = -0.0075;
static double ConfigCameraInertiaConsumeForward = -0.005;
static double ConfigCameraInertiaConsumeBackward = 0.005;
static double ConfigCameraTargetAmortizeFactor = 0.002;
static double ConfigCameraOrbitMagnification = 6;
/* ----------------------------------------------------- */

/* --------------- Object ------------------------------*/
static int ConfigObjectTexturePhiMax = 90;
static int ConfigObjectTextureThetaMax = 360;
static int ConfigObjectTextureStepLatitude = 3;
static int ConfigObjectTextureStepLongitude = 6;
static double ConfigObjectFullAngle = 4.0 * M_PI;
static double ConfigObjectFactorSize = 0.05;
static double ConfigObjectFactorSpeedScene = 1.0;
static double ConfigObjectFactorSpeedMaximum = 200000.0;
static double ConfigObjectFactorSpeedChangeFactor = 100.0;
/* ----------------------------------------------------- */


#endif //OMUAMUA_CONFIG_H

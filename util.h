//
// Created by glagos on 23-07-21.
//

#include <cglm/mat4.h>

#ifndef OMUAMUA_UTIL_H
#define OMUAMUA_UTIL_H

char* stringRepeat(char* s, int ntimes);

char *ltrim(char *s);
char *rtrim(char *s);
char *trim(char *s);

// Math
void HomogRotate3D(float angle, vec3* axis, mat4* matrix);
void Mat4Normal(mat4 m, mat3 dest);
void Translate3D(float tx, float ty, float tz, mat4 m);
int lenIntArray(int* array);

// File reading
char *textFileRead(char *fn);


#endif //OMUAMUA_UTIL_H

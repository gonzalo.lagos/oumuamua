//
// Created by glagos on 27-07-21.
//

#ifndef OMUAMUA_JSON_H
#define OMUAMUA_JSON_H

#include <jansson.h>
#include <stdio.h>
#include <stdlib.h>

#include "object.h"

/* forward refs */
void print_json(json_t *root, Object* object);
void print_json_aux(json_t *element, Object* object, const char* key);
const char *json_plural(size_t count);
void print_json_object(json_t *element, Object* object);
void print_json_array(json_t *element, Object* object);
void print_json_string(json_t *element);
void print_json_integer(json_t *element);
void print_json_real(json_t *element);
void print_json_true(json_t *element);
void print_json_false(json_t *element);
void print_json_null(json_t *element);
void parse_object(Object* object, const char* key, json_t* element);
void print_json_objects(json_t *element, Object* object, size_t len);


void parse_json_root_array(json_t* element);
void parse_json_string(json_t* element, char* string);
void parse_json_integer(json_t* element, int* integer);
void parse_json_real(json_t* element, double* real);
void parse_json_float(json_t* element, float* fp);
void parse_json_boolean(json_t* element, bool* boolean);

// static Object* stellar = 0;
// static Object* voidbox = 0;

#endif //OMUAMUA_JSON_H

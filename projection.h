//
// Created by glagos on 22-07-21.
//

#ifndef OMUAMUA_PROJECTION_H
#define OMUAMUA_PROJECTION_H

#include <cglm/cglm.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "window.h"


// ProjectionData  Maps data on scene projection
typedef struct ProjectionDataStruct {
    mat4    projection;
    int     projectionUniform;
} ProjectionData;

// InstanceProjection  Stores data on scene projection
static ProjectionData InstanceProjection;

ProjectionData* getProjection();
void createProjection(unsigned int program);
void bindProjection();

#endif //OMUAMUA_PROJECTION_H

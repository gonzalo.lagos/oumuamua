#include "object-element.h"

struct _OumuamuaObjectElement {
    float*  vertices;
    float*  verticeNormals;
    int*    indices;
    float*  textureCoords;
};

G_DEFINE_TYPE (OumuamuaObjecteElement, oumuamua_object_element, G_TYPE_OBJECT)


static void
oumuamua_object_element_class_init (OumuamuaObjectElementClass *klass)
{

}

static void
oumuamua_object_element_init (OumuamuaObjectElement *self)
{

}

OumuamuaObjectElement *
oumuamua_object_element_new (void)
{
  OumuamuaObjectElement *object;

  object = g_object_new (OUMUAMUA_TYPE_OBJECT_ELEMENT, NULL);
  return object;
}



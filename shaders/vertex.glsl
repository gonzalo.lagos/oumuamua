#version 330

uniform mat4 projectionUniform;
uniform mat4 cameraUniform;
uniform mat4 modelUniform;
uniform mat3 normalUniform;
uniform vec3 pointLightingLocationUniform;
uniform vec3 pointLightingColorUniform;
uniform int isLightEmitterUniform;

in vec3 vertexAttributes;
in vec3 vertexNormalAttributes;
in vec2 vertexTextureCoords;

out vec3 N;
out vec2 shaderTextureCoords;
out vec3 vertexLighting;


void main() {
    vec4 modelViewPosition = modelUniform * vec4(vertexAttributes, 1);
    gl_Position = projectionUniform * cameraUniform * modelViewPosition;
    shaderTextureCoords = vertexTextureCoords;
    // Process lighting
    if (isLightEmitterUniform == 1) {
        // Light emitter
        vec3 lightDirection = normalize(pointLightingLocationUniform - modelViewPosition.xyz);
        vec3 transformedNormal = normalUniform * vertexNormalAttributes;
        float directionalLighting = max(dot(transformedNormal, lightDirection), 0.0);
        vertexLighting = pointLightingColorUniform * directionalLighting;
    } else {
        // Light receiver
        vertexLighting = vec3(1.0, 1.0, 1.0);
    }
}

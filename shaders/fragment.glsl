#version 330

uniform sampler2D textureUniform;
uniform int isLightReceiverUniform;

in vec2 shaderTextureCoords;
in vec3 vertexLighting;

out vec4 objectColor;


void main() {
    vec4 objectColorTexture = texture(textureUniform, shaderTextureCoords);
    // Apply lighting to pixel
    if (isLightReceiverUniform == 1) {
        objectColor = vec4(objectColorTexture.rgb * vertexLighting, objectColorTexture.a);
    } else {
        objectColor = objectColorTexture;
    }
}
//
// Created by glagos on 15-07-21.
//

#include "circle.h"

#include <math.h>


Circle generateCircleFilledFromObject(Object *object) {
    return generateCircle(object->distance,
                          object->radius,
                          object->radiate);
}

Circle generateCircleFromObject(Object *object) {
    return generateCircle(object->radius, 0.0f, object->radiate);
}

Circle generateCircle(float radius, float thickness, bool radiate) {
    Circle circle;

    int i;
    int j;
    int k;
    int l;

    int accumulatorMainSize;
    int accumulatorIndicesSize;
    float normalDirection;

    float radiusInsideN;
    float radiusOutsideN;
    int nbVertices;

    double angle;
    int angleMax;


    angleMax = 360;
    if (thickness > 0) {
        accumulatorMainSize = 2;
        accumulatorIndicesSize = 6;
    } else {
        accumulatorMainSize = 1;
        accumulatorIndicesSize = 2;
    }

    circle.vertices = (float*) malloc(sizeof(float) * 3 * (angleMax+1) * accumulatorMainSize);
    circle.verticeNormals = (float*) malloc(sizeof(float) * 3 * (angleMax+1) * accumulatorMainSize);
    circle.indices = (int*) malloc(sizeof(int) * (angleMax + 1) * accumulatorIndicesSize);
    circle.textureCoords = (float*) malloc(sizeof(float) * 2 * (angleMax+1) * accumulatorMainSize);

    i = j = k = l = 0;
    nbVertices = 1;
    radiusInsideN = normalizeObjectSize(radius);
    radiusOutsideN = normalizeObjectSize(radius + thickness);

    // Normal is -1 if sun, which is the light source, to avoid any self-shadow effect
    if (radiate == true) {
        normalDirection = -1.0f;
    } else {
        normalDirection = 1.0f;
    }

    for (angle = 0.0; angle <= (double)angleMax; angle++) {
        // Generate inner circle object
        generateCircleObject(&circle, radiusInsideN, thickness,
                             angle, angleMax, normalDirection,
                             nbVertices, 0, &i, &j, &k, &l);

        if (thickness > 0.0) {
            // Generate outer circle object? (if not last)
            generateCircleObject(&circle, radiusOutsideN, thickness,
                                 angle, angleMax, normalDirection,
                                 nbVertices, 1, &i, &j, &k, &l);
            nbVertices++;
        }

        nbVertices++;
    }

    return circle;
}

void
generateCircleObject(Circle *circle, float radiusN, float thickness,
                     double angle, int angleMax, float normalDirection,
                     int nbVertices, int passIndex, int *i, int *j, int *k, int *l) {

    float vertexPositionX;
    float vertexPositionY;
    float vertexPositionZ;

    // Generate inside circle vertices
    vertexPositionX = (float)(cos(ConfigMathDegreeToRadian * angle));
    vertexPositionY = 0.0f;
    vertexPositionZ = (float)(sin(ConfigMathDegreeToRadian * angle));

    // Bind inside circle vertices
    circle->vertices[*i] = radiusN * vertexPositionX;
    circle->vertices[*i + 1] = radiusN * vertexPositionY;
    circle->vertices[*i + 2] = radiusN * vertexPositionZ;

    *i += 3;

    // Bind circle vertice normals
    circle->verticeNormals[*j] = normalDirection * vertexPositionX;
    circle->verticeNormals[*j + 1] = normalDirection * vertexPositionY;
    circle->verticeNormals[*j + 2] = normalDirection * vertexPositionZ;

    *j += 3;

    // Bind circle indices
    if (thickness > 0.0) {
        circle->indices[*k] = (nbVertices % (angleMax * 2)) + 1;
        circle->indices[*k + 1] = ((nbVertices + 1 + passIndex) % (angleMax * 2)) + 1;
        circle->indices[*k + 2] = ((nbVertices + 3) % (angleMax * 2)) + 1;

        *k += 3;
    } else {
        circle->indices[*k] = ((nbVertices) % angleMax) + 1;
        circle->indices[*k + 1] = ((nbVertices + 1) % angleMax) + 1;

        *k += 2;
    }

    // Bind circle texture coordinates
    circle->textureCoords[*l] = (float)passIndex;
    circle->textureCoords[*l + 1] = 0.0f;

   *l += 2;
}




//
// Created by glagos on 17-07-21.
//

#include "shader.h"

#include "light.h"
#include "matrix.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>


ShaderData *getShader() {
    return &InstanceShader;
}

void initializeShaders(unsigned int program) {
    ShaderData* shader = getShader();

    // Bind buffer to shaders attributes
    shader->vertexAttributes = (unsigned int)glGetAttribLocation(program, "vertexAttributes\x00");
    glEnableVertexAttribArray(shader->vertexAttributes);

    shader->normalAttributes = (unsigned int) glGetAttribLocation(program, "vertexNormalAttributes\x00");
    glEnableVertexAttribArray(shader->normalAttributes);

    shader->vertexTextureCoords = (unsigned int)glGetAttribLocation(program, "vertexTextureCoords\x00");
    glEnableVertexAttribArray(shader->vertexTextureCoords);

    // Bind misc. shaders uniforms
    setLightUniforms(program);
    setMatrixUniforms(program);
}



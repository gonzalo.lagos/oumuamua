    //
// Created by glagos on 21-07-21.
//

#ifndef OMUAMUA_BUFFER_H
#define OMUAMUA_BUFFER_H

#include "texture.h"
#include "object.h"

typedef struct BuffersStruct {
    ObjectElement   element;
    Texture         texture;

    float           angleRotation;
    float           angleRevolution;
    float           angleTilt;

    unsigned int    VBOElementVertices;
    unsigned int    VBOElementVerticesNormals;
    unsigned int    VBOElementTexture;
    unsigned int    VBOElementIndices;
} Buffers;

typedef struct BuffersMapElementStruct {
    Buffers* buffer;
    char id[250];
} BuffersMapElement;

typedef struct BuffersMapStruct {
    BuffersMapElement elements[100000];
    int counter;
} BuffersMap;


static BuffersMap InstanceBuffers;


BuffersMap* getMap();
void addToMap(Buffers* buffers, char* id);
Buffers* getBuffersFromMap(char* id);

void addToBufferAngleRotation(Buffers* buffers, float angle);
void addToBufferAngleRevolution(Buffers* buffers, float angle);
void createAllBuffers(Object* objects, unsigned int program, unsigned int vao);
void createBuffers(Object* object, unsigned int program, unsigned int vao);


#endif //OMUAMUA_BUFFER_H

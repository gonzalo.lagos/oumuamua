//
// Created by glagos on 17-07-21.
//

#ifndef OMUAMUA_SHADER_H
#define OMUAMUA_SHADER_H


#include <GL/glew.h>
#include <GLFW/glfw3.h>

typedef struct ShaderDataStruct {
    unsigned int vertexAttributes;
    unsigned int normalAttributes;
    unsigned int vertexTextureCoords;
} ShaderData;

static ShaderData InstanceShader;

static char* VertexShaderFilename = "./shaders/vertex.glsl";
static char* FragmentShaderFilename = "./shaders/fragment.glsl";

ShaderData* getShader();
void initializeShaders(unsigned int program);



#endif //OMUAMUA_SHADER_H

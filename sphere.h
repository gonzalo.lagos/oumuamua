//
// Created by glagos on 14-07-21.
//

#ifndef OMUAMUA_SPHERE_H
#define OMUAMUA_SPHERE_H


#include "object.h"

typedef ObjectElement Sphere;

Sphere generateSphereFromObject(Object* object);
Sphere generateSphere(float radius, float compression, bool radiate);



#endif //OMUAMUA_SPHERE_H

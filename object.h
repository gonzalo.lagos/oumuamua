//
// Created by glagos on 14-07-21.
//

#ifndef OMUAMUA_OBJECT_H
#define OMUAMUA_OBJECT_H

#include <stdbool.h>
#include <glib-object.h>
#include <glib.h>
#include "config.h"

G_BEGIN_DECLS

#define OUMUAMUA_TYPE_OBJECT  (oumuamua_object_get_type ())
G_DECLARE_FINAL_TYPE (OumuamuaObject, oumuamua_object, OUMUAMUA, OBJECT, GObject)


OumuamuaObject               *oumuamua_object_new                   (void);
OumuamuaObject               *oumuamua_object_load_objects          (gchar* mapName);
OumuamuaObject               *oumuamua_object_load_stellar          (void);
OumuamuaObject               *oumuamua_object_load_voidbox          (void);
void                          oumuamua_object_render_objects        (GList* objects,
                                                                     unsigned int program);
float                         oumuamua_object_normalize_size        (float size);
unsigned int                  oumuamua_object_get_draw_mode(Object* object);


G_END_DECLS










#endif //OMUAMUA_OBJECT_H

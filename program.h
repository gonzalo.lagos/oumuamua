//
// Created by glagos on 23-07-21.
//

#ifndef OMUAMUA_PROGRAM_H
#define OMUAMUA_PROGRAM_H


unsigned int newProgram(const char* vertexShaderSource, const char* fragmentShaderSource);
unsigned int compileShader(const char* source, unsigned int shaderType);


#endif //OMUAMUA_PROGRAM_H

//
// Created by glagos on 14-07-21.
//

#include "texture.h"

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <jpeglib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>


const int FILENAME_DEFAULT_SIZE = 200;
char* ASSETS_DIRECTORY = "assets";
char* FALLBACK_TEXTURE = "default";



Texture loadTexture(const char* name) {
    if (name == NULL) {
        fprintf(stderr, "No name provided for texture\n");
        exit(-1);
    }

    Texture texture = {};
    FILE* file;



    // Try JPG version of texture (most used)
    const char* jpgFilename = getPathFilename(ASSETS_DIRECTORY,
                                              name,
                                              JPG);
    // Checking existence
    if (probeFile(jpgFilename) == NOT_FOUND) {
        fprintf(stderr, "No JPEG file found\n");
        fprintf(stderr, "Trying PNG instead\n");
    } else {
        fprintf(stdout, "JPEG file found\n");
        // Starting JPEG decompression.
        unsigned char* image;
        int width, height, depth;
        int i;
        file = openFile(jpgFilename);

        struct jpeg_decompress_struct cinfo;
        struct jpeg_error_mgr jerr;
        JSAMPROW rowpointer[1];
        unsigned long location;
        cinfo.err = jpeg_std_error(&jerr);

        jpeg_create_decompress(&cinfo);
        jpeg_stdio_src(&cinfo, file);
        jpeg_read_header(&cinfo, 0);
        cinfo.scale_num = 1;
        cinfo.scale_denom = 1;

        jpeg_start_decompress(&cinfo);
        width = cinfo.output_width;
        height = cinfo.output_height;
        depth = cinfo.num_components;
        image = (unsigned char*) malloc(width * height * depth);
        rowpointer[0] = (unsigned char*) malloc(width * depth);
        // read one scan line at a time
        while (cinfo.output_scanline < cinfo.output_height) {
            jpeg_read_scanlines(&cinfo, rowpointer, 1);
            for (i = 0; i < (width * depth); i++)
                image[location++] = rowpointer[0][i];
        }

        FileOps op = closeFile(file);
        if (op == CLOSE_ERROR) {
            fprintf(stderr, "Error while closing file: %s\n", jpgFilename);
        }

        jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);

        glGenTextures(1, &texture.ref);
        glActiveTexture(texture.ref);
        glBindTexture(GL_TEXTURE_2D, texture.ref);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                     width, height, 0,
                     GL_RGBA, GL_UNSIGNED_BYTE, image);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        glBindTexture(GL_TEXTURE_2D, 0);

        free(rowpointer[0]);
    }

    // Try PNG version of texture
    const char* pngFilename = getPathFilename(ASSETS_DIRECTORY,
                                              name,
                                              PNG);
    // Checking existence
    if (probeFile(pngFilename) == NOT_FOUND) {

    } else {
        fprintf(stderr, "No PNG file found\n");
        fprintf(stderr, "Trying fallback texture instead\n");
    }

    // Try fallback texture
    const char* fallbackFilename = getPathFilename(ASSETS_DIRECTORY,
                                                   FALLBACK_TEXTURE,
                                                   PNG);
    // Checking existence
    if (probeFile(fallbackFilename) == NOT_FOUND) {
        // Open default texture (default object color)

    } else {
        fprintf(stderr, "Failed opening fallback texture file\n");
        fprintf(stderr, "Cannot continue. Exiting...\n");
        exit(-1);
    }




    /* TODO: make helper function for formatting filename and format
             according to an Enum (JPG|PNG).
    */


}

const char* getPathFilename(char* path, const char *name, ImageFileFormat format) {

    char* filepath = (char*) malloc(sizeof(char) * FILENAME_DEFAULT_SIZE);
    sprintf(filepath, "%s/%s", path, name);
    switch (format) {
        case JPG: strcat(path, ".jpg");
            break;
        case PNG: strcat(path, ".png");
            break;
        default:
            break;
    }

    return filepath;
}

FileOps probeFile(const char *filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) {
        return NOT_FOUND;
    } else {
        return EXISTS;
    }
    fclose(file);
}

FILE* openFile(const char *filename) {
    FILE* file = fopen(filename, "rb");
    return file;
}

FileOps closeFile(FILE *file) {
    int op = fclose(file);
    if (op == EOF) {
        return CLOSE_ERROR;
    } else {
        return CLOSED_FINE;
    }
}

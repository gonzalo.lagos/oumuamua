//
// Created by glagos on 20-07-21.
//

#include "light.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>


LightData* getLight() {
    return &InstanceLightData;
}

void setLightUniforms(unsigned int program) {
    LightData *light = getLight();

    light->pointLightingLocationUniform =
            glGetUniformLocation(program, "pointLightingLocationUniform\x00");
    light->pointLightingColorUniform =
            glGetUniformLocation(program, "pointLightingColorUniform\x00");

    light->isLightEmmiterUniform =
            glGetUniformLocation(program, "isLightEmitterUniform\x00");
    light->isLightReceiverUniform =
            glGetUniformLocation(program, "isLightReceiverUniform\x00");

}

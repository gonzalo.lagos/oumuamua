//
// Created by glagos on 14-07-21.
//

#ifndef OMUAMUA_EVENT_H
#define OMUAMUA_EVENT_H

#include <stdbool.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// EventKeyState  Maps input key states
typedef struct EventKeyStateStruct {
    bool    moveTurbo;

    bool    moveUp;
    bool    moveDown;
    bool    moveRight;
    bool    moveLeft;

    float   watchX;
    float   watchY;
} EventKeyState;

// InstanceEventKeyState  Stores input key states
static EventKeyState InstanceEventKeyState = {
        false,
        false,
        false,
        false,
        false,
        0.0f,
        0.0f
};

EventKeyState* getEventKeyState();
void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods);
void handleMouseCursor(GLFWwindow* window, double positionX, double positionY);
void handleMouseScroll(GLFWwindow* window, double offsetX, double offsetY);
void resetMouseCursor();




#endif //OMUAMUA_EVENT_H

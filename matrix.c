//
// Created by glagos on 20-07-21.
//

#include "matrix.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

static int stack_counter ;

void initializeMatrix() {
    mat4 identityMatrix;
    glm_mat4_identity(identityMatrix);

    MatrixStack* stack = (MatrixStack*) calloc(MatrixStackLimit, sizeof(MatrixStack));
    // TODO: Create a Matrix Stack data structure
    setMatrixStack(stack);
    setMatrix(&identityMatrix);
}

mat4 *getMatrix() {
    return InstanceMatrixStack.current;
}

void setMatrix(mat4 *matrix) {
    InstanceMatrixStack.current = matrix;
}

MatrixStack *getMatrixStack() {
    return InstanceMatrixStack.stack;
}

void setMatrixStack(MatrixStack *matrixStack) {
    InstanceMatrixStack.stack = matrixStack;
}

void pushMatrix() {
    // Stack current matrix
    mat4* currentMatrix = getMatrix();
    getMatrixStack()->stack[stack_counter++];
    // Generate new current matrix
    mat4 identityMatrix, newMatrix;
    glm_mat4_identity(identityMatrix);
    glm_mat4_mul(identityMatrix, *currentMatrix, newMatrix);
    setMatrix(&newMatrix);
}

void popMatrix() {
    if (stack_counter == 0) {
        fprintf(stderr, "Cannot pop: matrix stack is empty");
        exit(-1);
    }

    MatrixStack lastElement = getMatrixStack()->stack[stack_counter];
    mat4* previousMatrix = lastElement.current;
    // Assign now-popped element
    setMatrix(previousMatrix);
    // Remove this element from the stack
    getMatrixStack()->stack[stack_counter--];
}

MatrixUniforms *getMatrixUniforms() {
    return &InstanceMatrixUniforms;
}

void setMatrixUniforms(unsigned int program) {
    MatrixUniforms* matrixUniforms = getMatrixUniforms();

    matrixUniforms->model = glGetUniformLocation(program, "modelUniform\x00");
    matrixUniforms->normal = glGetUniformLocation(program, "normalUniform\x00");
    matrixUniforms->texture = glGetUniformLocation(program, "textureUniform\x00");
}






//
// Created by glagos on 22-07-21.
//

#ifndef OMUAMUA_ORBIT_H
#define OMUAMUA_ORBIT_H

#include "object.h"

void createOrbitTraces(Object* objects, unsigned int program, unsigned int vao);

#endif //OMUAMUA_ORBIT_H

//
// Created by glagos on 14-07-21.
//

#include "sphere.h"

Sphere generateSphereFromObject(Object *object) {
    return generateSphere(object->radius, object->compression, object->radiate);
}

Sphere generateSphere(float radius, float compression, bool radiate) {
    Sphere  sphere;

    int     i;
    int     j;
    int     k;
    int     l;

    float   normalDirection;
    float   radiusN;
    int     nbVertices;

    int     unarySizeFull;
    int     unarySizeShort;

    int     resLongitude;
    int     longitude;
    int     latitude;
    double  longitudeRF;
    double  latitudeRF;

    float   vertexPositionX;
    float   vertexPositionY;
    float   vertexPositionZ;


    unarySizeFull = (2 * ConfigObjectTexturePhiMax / ConfigObjectTextureStepLatitude + 1)
                    * (ConfigObjectTextureThetaMax / ConfigObjectTextureStepLongitude + 1);

    unarySizeShort = (2 * ConfigObjectTexturePhiMax / ConfigObjectTextureStepLatitude)
                    * (ConfigObjectTextureThetaMax / ConfigObjectTextureStepLongitude);

    sphere.vertices         = (float*) calloc(3 * unarySizeFull, sizeof(float));
    sphere.verticeNormals   = (float*) calloc(3 * unarySizeFull, sizeof(float));
    sphere.indices          = (int*) calloc(6 * unarySizeShort, sizeof(int));
    sphere.textureCoords    = (float*) calloc(2 * unarySizeFull, sizeof(float));

    i = j = k = l = 0;

    radiusN = normalizeObjectSize(radius);
    nbVertices = 0;
    resLongitude = (int)((float)(ConfigObjectTextureThetaMax) / (float)(ConfigObjectTextureStepLongitude) + 1.0f);

    // Normal is -1 if sun, which is the light source, to avoid any self-shadow effect
    if (radiate == true) {
        normalDirection = -1.0f;
    } else {
        normalDirection = 1.0f;
    }

    // Map sphere data
    for (latitude = -90; latitude <= ConfigObjectTexturePhiMax; latitude += ConfigObjectTextureStepLatitude) {
        for (longitude = 0; longitude <= ConfigObjectTextureThetaMax; longitude += ConfigObjectTextureStepLongitude) {

            // Convert latitude & longitude to radians
            longitudeRF = (double)(ConfigMathDegreeToRadian) * (double)(longitude);
            latitudeRF  = (double)(ConfigMathDegreeToRadian) * (double)(latitude);

            // Process vertex positions
            vertexPositionX = (float)(sin(longitudeRF) * cos(latitudeRF));
            vertexPositionY = (float)(sin(latitudeRF) * compression);
            vertexPositionZ = (float)(cos(latitudeRF) * cos(longitudeRF));

            // Bind sphere vertices
            sphere.vertices[i] = radiusN * vertexPositionX;
            sphere.vertices[i + 1] = radiusN * vertexPositionY;
            sphere.vertices[i + 2] = radiusN * vertexPositionZ;

            i += 3;

            // Bind sphere vertices normals
            sphere.verticeNormals[j] = normalDirection * vertexPositionX;
            sphere.verticeNormals[j + 1] = normalDirection * vertexPositionY;
            sphere.verticeNormals[j + 2] = normalDirection * vertexPositionZ;

            j += 3;

            // Bind sphere indices
            if (longitude != ConfigObjectTextureThetaMax && latitude < ConfigObjectTexturePhiMax) {

                sphere.indices[k] = nbVertices;
                sphere.indices[k + 1] = nbVertices + 1;
                sphere.indices[k + 2] = nbVertices + 1 + resLongitude;

                sphere.indices[k + 3] = nbVertices;
                sphere.indices[k + 4] = nbVertices + 1 + resLongitude;
                sphere.indices[k + 5] = nbVertices + resLongitude;

                k += 6;
            }

            nbVertices++;

            // Bind sphere texture coordinates
            sphere.textureCoords[l] = (float)(longitude) / (float)(ConfigObjectTextureThetaMax);
            sphere.textureCoords[l + 1] = -1.0f * (float)(90.0 + latitude) / (float)(90.0 + ConfigObjectTexturePhiMax);

            l += 2;
        }
    }

    return sphere;
}


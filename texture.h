//
// Created by glagos on 14-07-21.
//

#ifndef OMUAMUA_TEXTURE_H
#define OMUAMUA_TEXTURE_H

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <jpeglib.h>
#include <jpegint.h>
#include <inttypes.h>


typedef struct TextureStruct {
    GLuint ref;
} Texture;

typedef enum ImageFileFormatEnum {
    JPG,
    PNG
} ImageFileFormat;

typedef enum FileOpsEnum {
    NOT_FOUND,
    EXISTS,
    CLOSED_FINE,
    CLOSE_ERROR
} FileOps;

Texture loadTexture(const char* name);
const char* getPathFilename(char* path, const char* name, ImageFileFormat format);
FileOps probeFile(const char* filename);
FILE* openFile(const char* filename);
FileOps closeFile(FILE* file);

#endif //OMUAMUA_TEXTURE_H

//
// Created by glagos on 14-07-21.
//

#include "speed.h"

#include <threads.h>


struct Speed* getSpeed() {
    return &InstanceSpeed;
}

void setFramerate(struct Speed *speed, int framerate) {
    speed->framerate = (double)framerate;
}

void updateSpeedFactor(double factorOffset) {
    getSpeed()->factor += factorOffset * ConfigObjectFactorSpeedChangeFactor;
    // Cap down to zero? (prevents negative or very-high speeds)
    if (getSpeed()->factor < 0) {
        getSpeed()->factor = 0;
    } else if (getSpeed()->factor > ConfigObjectFactorSpeedMaximum) {
        getSpeed()->factor = ConfigObjectFactorSpeedMaximum;
    }
}

void updateElapsedTime(double nowTime) {
    struct Speed* speed = getSpeed();

    speed->timeElapsed = nowTime - speed->timePrevious;
    speed->timePrevious = nowTime;
}

void deferSceneUpdate() {
    struct timespec ts = {
            .tv_sec = 1 / (getSpeed()->framerate * ConfigSpeedFramerateFactor),
            .tv_nsec = 0
    };
    nanosleep(&ts, NULL);
}

float angleSince(float angleTime, double factor, double elapsed) {
    // angleTime in milliseconds
    // elapsed in milliseconds
    //  -> angle = (elapsed / angleTime) * ConfigObjectFullAngle
    // Important: cap angle value (circle from 0 to 360 w/ modulus)
    if (angleTime == 0) {
        return 0.0f;
    }

    return (float)(modf(((ConfigObjectFactorSpeedScene * factor * elapsed) / (double)(angleTime) * ConfigObjectFullAngle),
                        &ConfigObjectFullAngle));
}

float revolutionAngleSince(Object *object, double factor, double elapsed) {
    return angleSince(object->revolution * (float)(ConfigTimeYearToMilliseconds),
                      factor,
                      elapsed * (double)(ConfigTimeSecondToMilliseconds));
}

float rotationAngleSince(Object* object, double factor, double elapsed) {
    return angleSince(object->rotation * (float)(ConfigTimeDayToMilliseconds),
                      factor,
                      elapsed * (double)(ConfigTimeSecondToMilliseconds));
}

float revolutionAngleSinceLast(Object *object) {
    return revolutionAngleSince(object, getSpeed()->factor, getSpeed()->timeElapsed);
}

float rotationAngleSinceLast(Object *object) {
    return rotationAngleSince(object, getSpeed()->factor, getSpeed()->timeElapsed);
}

float revolutionAngleSinceStart(Object *object) {
    return revolutionAngleSince(object, 1.0, (double)(ConfigTimeStartFromMilliseconds));
}

float rotationAngleSinceStart(Object *object) {
    return rotationAngleSince(object, 1.0, (double)(ConfigTimeStartFromMilliseconds));
}


float normalizedTimeFactor() {
    float f = ConfigTimeNormalizeFactor * (float)(getSpeed()->timeElapsed);
    return f;
}

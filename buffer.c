//
// Created by glagos on 21-07-21.
//

#include "buffer.h"
#include "speed.h"
#include "circle.h"
#include "sphere.h"
#include <string.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

/*
BuffersMap* getMap() {
    BuffersMap* bm = (BuffersMap*) malloc(sizeof(BuffersMap));
    bm->capacity = 10000;
    bm->counter = 0;
    bm->elements = (BuffersMapElement*) calloc(10000, sizeof(BuffersMapElement));
}
*/


void addToMap(Buffers *buffers, char *id) {
    BuffersMapElement bme;
    bme.buffer = buffers;
    strcpy(bme.id, id);

    InstanceBuffers.elements[InstanceBuffers.counter++] = bme;
}

BuffersMap *getMap() {
    return &InstanceBuffers;
}

Buffers *getBuffersFromMap(char *id) {
    int limit = InstanceBuffers.counter;
    for (int i = 0; i < limit; i++) {
        BuffersMapElement bme = InstanceBuffers.elements[i];
        if (strcmp(bme.id, id) == 0)
            return bme.buffer;
    }

    return NULL;
}

void addToBufferAngleRotation(Buffers *buffers, float angle) {
    buffers->angleRotation += angle;
}

void addToBufferAngleRevolution(Buffers *buffers, float angle) {
    buffers->angleRevolution += angle;
}

void createAllBuffers(Object *objects, unsigned int program, unsigned int vao) {
    // Object buffers
    if (objects == NULL) {
        fprintf(stderr, "objects of type Object, argument is null.");
        exit(-1);
    }

    int i = 0;
    Object* o = objects;
    while (o != NULL) {
        createBuffers(o, program, vao);
        o = objects + (++i);
    }
}

void createBuffers(Object *object, unsigned int program, unsigned int vao) {

    Buffers b;

    // Zero angle
    b.angleRotation     = rotationAngleSinceStart(object);
    b.angleRevolution   = revolutionAngleSinceStart(object);
    b.angleTilt         = object->tilt * (float)ConfigMathDegreeToRadian;

    // Generate object
    if (strcmp(object->type, "sphere") == 0) {
        b.element = (ObjectElement) generateSphereFromObject(object);
    } else if (strcmp(object->type, "circle") == 0) {
        b.element = (ObjectElement) generateCircleFromObject(object);
    } else if (strcmp(object->type, "circle-filled") == 0) {
        b.element = (ObjectElement) generateCircleFilledFromObject(object);
    } else {
        fprintf(stderr, "Object type not supported @createBuffers.\n");
        exit(-1);
    }

    // Load texture
    b.texture = loadTexture(object->name);
    // Color storage
    glBindFragDataLocation(program, 0, "objectColor\x00");

    // Bind object to VAO
    glBindVertexArray(vao);

    // Count length
    int lenVertices = 1, lenVerticesNormals = 1, lenTexture = 1, lenIndices = 1;
    while (b.element.vertices + lenVertices != NULL) lenVertices++;
    while (b.element.verticeNormals + lenVerticesNormals != NULL) lenVerticesNormals++;
    while (b.element.textureCoords + lenTexture != NULL) lenTexture++;
    while (b.element.indices + lenIndices != NULL) lenIndices++;

    // Create the VBO
    glGenBuffers(1, &b.VBOElementVertices);
    glBindBuffer(GL_ARRAY_BUFFER, b.VBOElementVertices);
    glBufferData(GL_ARRAY_BUFFER, lenVertices * 4, b.element.vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &b.VBOElementVerticesNormals);
    glBindBuffer(GL_ARRAY_BUFFER, b.VBOElementVerticesNormals);
    glBufferData(GL_ARRAY_BUFFER, lenVerticesNormals * 4, b.element.verticeNormals, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &b.VBOElementTexture);
    glBindBuffer(GL_ARRAY_BUFFER, b.VBOElementTexture);
    glBufferData(GL_ARRAY_BUFFER, lenTexture * 4, b.element.textureCoords, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &b.VBOElementIndices);
    glBindBuffer(GL_ARRAY_BUFFER, b.VBOElementIndices);
    glBufferData(GL_ARRAY_BUFFER, lenIndices * 4, b.element.indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Go deeper (if any child)
    Object* o = (Object *) object->objects;
    int objectCounter = 0;
    while ((o + objectCounter++) != NULL) {
        createBuffers(o + objectCounter, program, vao);
    }

    // Store buffers
    addToMap(&b, object->name);
}






